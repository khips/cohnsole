"""
A little plugin for sending code from SublimeText to Cohlua.
"""
import sublime, sublime_plugin

import json
import threading
import select
import socket
import struct
import re
from functools import partial
from Queue import Queue, Empty

ClientThreads = {}

def get_client_thread(view, on_error=None, on_success=None, host="localhost", port=3240):
	"""
	Returns the client thread associated with a view, if it exists.
	If it does not exist, creates the new client thread before
	returning it.
	"""
	vid = view.id()

	if not ClientThreads.get(vid):
		newthread = ManicThread(view, on_error, on_success)
		newthread.start_when_ready()
		ClientThreads[vid] = newthread

	return ClientThreads[vid]

def is_connected(view):
	return bool(ClientThreads.get(view.id()))

def is_lua_view(view):
	return view.score_selector(1, "source.lua") > 0 and	not is_connected(view)

OutputPanel = None

def display_cohnsole_output(view, text=""):
	# Set up output panel:
	window = view.window()
	if not window:
		sublime.status_message(text)
		return

	global OutputPanel
	if not OutputPanel:
		OutputPanel = window.get_output_panel("cohnsole") 

	output = OutputPanel
	output.set_read_only(False)

	# Edit contents
	edit = output.begin_edit()
	output.insert(edit, output.size(), text)
	output.insert(edit, output.size(), "\n")
	output.end_edit(edit)

	output.set_read_only(True)
	output.show(output.size())

	window.run_command("show_panel", {"panel": "output.cohnsole"})

class Outputter:
	def show_text(self, t):
		sublime.set_timeout(lambda: display_cohnsole_output(self.view, t), 10)

class ManicThread(threading.Thread, Outputter):
	def __init__(self, view, on_error=None, on_success=None, on_disconnect=None):
		super(ManicThread, self).__init__()
		settings = view.settings()
		self.view = view

		self.raw = False

		self._host = None
		self._port = None
		self.host = settings.get("host")
		self.port = settings.get("port")
		settings.add_on_change("host", self.on_host_change)
		settings.add_on_change("port", self.on_port_change)

		self._queue = Queue()
		self._stopped = threading.Event()

		self._on_error = on_error
		self._on_success = on_success
		self._on_disconnect = on_disconnect
		self.is_connected = False

	def start_when_ready(self):
		if not self.host or not self.port:
			window = self.view.window()
			settings = self.view.settings()
			def on_done_port(arg):
				settings.set("port", arg)
				self.port = int(arg)
				self.start()

			def on_done_host(arg):
				settings.set("host", arg)
				self.host = arg
				window.show_input_panel("Port:", "3240", on_done_port,
										None, None)

			window.show_input_panel("Host:", "localhost", on_done_host, None, None)
		else:
			self.start()


	def __nonzero__(self):
		return self.is_connected

	@property
	def port(self):
		return self._port

	@port.setter
	def port(self, port_):
		port_ = port_ and int(port_)
		if self._port != port_:
			self._port = port_
			print("Port changed")

	@property
	def host(self):
		return self._host

	@host.setter
	def host(self, host_):
		if self._host != host_:
			self._host = host_
			print("Host changed")


	def on_host_change(self):
		self.host = self.view.settings().get("host")

	def on_port_change(self):
		self.port = self.view.settings().get("port")
		print("Port changed")

	# Socket/lifetime callbacks:
	def on_disconnect(self):
		"""
		Called when the client disconnects, cleanly or not.
		"""
		if callable(self._on_disconnect):
			self._on_disconnect()
		else:
			self.show_text("Disconnected")

	def on_success(self):
		"""
		Called after successfully connecting to the server.
		"""
		if callable(self._on_success):
			self._on_success(self)
		else:
			self.show_text("Connected to %s:%s." % (self.host, self.port))

	def on_error(self, message):
		"""
		Called if a connection could not be established.
		"""
		if callable(self._on_error):
			self._on_error(self.host, self.port, message)
		else:
			self.show_text("[!!] %s" % message)

	# Communication:
	def receive_message(self):
		(dlen, ) = struct.unpack(">H", self.client.recv(2))
		if dlen == 0:
			data = ""
		else:
			data = self.client.recv(dlen)
		return data

	def process_message(self, message):
		"""
		If 'raw' is set to True, run this method on the message
		before passing it to the handler.
		"""
		return json.loads(message)

	def run(self):
		try:
			client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			client.connect((self.host, self.port))
			self.client = client
		except socket.error as (value, message):
			self.on_error(message)
			return
		else:
			self.is_connected = True
			self.on_success()

		data = self.receive_message()
		self.show_text(data)

		# use select.select?

		while not self._stopped.isSet():
			# Receive messages to send.
			try:
				next, callback = self._queue.get(timeout=1)
			except Empty:
				continue
			
			try:
				print("Sending command: %s" % next)
				client.send(next)

				data = self.receive_message()
				if not self.raw:
					data = self.process_message(data)
			except socket.error:
				self.is_connected = False
				self.on_error(message)
				return
			except Exception as exc:
				self.on_error("Program error: %s" % exc)
				break

			if callback:
				callback(data)

		client.close()
		self.is_connected = False
		self.on_disconnect()

	def stop(self):
		"""
		Disconnect from the server.
		"""
		self._stopped.set()

	def send_command(self, cmd, callback=None):
		self._queue.put(("%s\r\n" % cmd,  callback))




############################################################
## Commands:

class StartCohnsole(sublime_plugin.TextCommand, Outputter):
	"""
	Must be run before any of the other Cohnsole commands.
	"""
	def __init__(self, *args):
		super(StartCohnsole, self).__init__(*args)
		self.connecting = False	

	def run(self, edit):
		client = get_client_thread(self.view)

	def description(self):
		return "Connects to a running cohnsole connection."

	def is_enabled(self):
		"There must be no open connection associated with this view."
		return not is_connected(self.view)

class CloseCohnsole(sublime_plugin.TextCommand, Outputter):
	def is_enabled(self):
		"Can't close a connection that hasn't been opened!"
		return is_connected(self.view)

	def run(self, edit):
		connection = ClientThreads.get(self.view.id())
		if connection:
			connection.stop()

class SendToCohnsole(sublime_plugin.TextCommand, Outputter):
	"""
	Send the selected text over the port.
	"""
	def run(self, edit):
		view = self.view

		# Fetch the selected text, but expand it to complete lines.
		region = view.full_line(view.sel()[0])
		source = view.substr(region)
		source = re.sub(r"[\r\n]+", " ", source)	# this does not handle multiline Lua strings
		client = get_client_thread(view)
		client.send_command(source, self.handle_return_value)
		self.show_text("Sent %s character(s)" % len(source))

	def handle_return_value(self, data):
		sublime.set_timeout(lambda: self._handle_return_value(data), 5)

	def _handle_return_value(self, data):
		settings = self.view.settings()
		if not data:
			self.show_text("[!!] Empty return value.")
		elif not data["success"]:
			mesg = "[!!] Evaluation error: %s" % data.get("output", "--")
			self.show_text(mesg)
		elif settings.get("showraw", False):
			self.show_text(str(data))
		else:
			if settings.get("showbuffer", True):
				if isinstance(data.get("buffer"), list):
					for line in data["buffer"]:
						self.show_text("%% %s" % line)
			if data.get("output"):
				self.show_text(data["output"])

	def is_enabled(self):
		return is_connected(self.view)

class ManicExamineVariable(sublime_plugin.TextCommand, Outputter):
	"""
	Retrieve the value stored in a global variable and display it
	in an output panel.
	"""
	def handle_return_value(self, data):
		if not data["success"]:
			self.show_text("Error: %s" % data)
		else:
			self.show_text("Current value for %s: %s" % (data["name"], data["value"]))

	def run(self, edit):
		view = self.view
		client = get_client_thread(view)
		word = view.substr(view.word(view.sel()[0])).split(" ", 1)[0]

		client.send_command("%x " + word, self.handle_return_value)

	def is_enabled(self):
		return is_connected(self.view)

CommentPattern = re.compile(r"(\w+)=(\S+)")

class PrintCurrentScope(sublime_plugin.ApplicationCommand):
	def run(self):
		view = sublime.active_window().active_view()
		scope = view.scope_name(view.sel()[0].a)
		sublime.status_message(scope)


class StopAllCohnsoleThreads(sublime_plugin.ApplicationCommand):
	def run(self):
		for thrd in threading.enumerate():
			if isinstance(thrd, ManicThread):
				print("Waiting for thread to stop...")
				thrd.stop()
				thrd.join()
		print("Done.")

class EvaluateAndReplace(sublime_plugin.TextCommand):
	"""
	Sends the highlighted text over the connection and replaces it
	with the return value, if applicable.
	"""
	def run(self):
		view = self.view
		client = get_client_thread(view)
		for sel in view.sel():
			expression = view.substr(sel)
			client.send_command("%eval", partial(self.handle_return_value, sel))

	def handle_return_value(self, sel, lines):
		"""
		Gets a list of results, one for each evaluated line in the 
		buffer.  Replaces each line in sel with its corresponding
		value in lines.
		"""
		view = self.view
		edit = view.begin_edit("replacements")
		for region, replace in zip(sel, lines):
			view.replace(edit, region, replace)

		view.end_edit(edit)

	def is_enabled(self):
		return is_connected(self.view)

class CohnsoleListener(sublime_plugin.EventListener):
	"""
	Search the comments for cohlua modelines.
	This could be enhanced and pulled out into its own plugin.
	"""
	def is_enabled(self):
		return is_lua_view(sublime_plugin.active_window().active_view())

	def load_settings(self, view):
		# Read in cohnsole 'modelines'.
		settings = view.settings()
		comments = view.find_by_selector("comment")
		for comment in comments:
			comment_text = view.substr(comment).strip()
			offset = comment_text.find("cohlua:")
			if offset < 0:
				continue
			comment_text = comment_text[offset+7:]

			for setting, val in CommentPattern.findall(comment_text):
				settings.set(setting, val)

	def check_autoload(self, view):
		"""
		Check if the 'autoload' setting is set to a 'truthy' value
		for this file.  If it is, reload the contents of this file
		into the running session.
		"""
		settings = view.settings()
		autoload = settings.get("autoload", "")
		autoload = re.match(r"[y1]", autoload.strip())

		if autoload:
			# TODO: Reload code.
			pass

	def on_load(self, view):
		self.load_settings(view)

	def on_post_save(self, view):
		self.load_settings(view)

		self.check_autoload(view)
